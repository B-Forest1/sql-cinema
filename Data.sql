Use `Cinema`;

DELETE FROM Salle;
DELETE FROM Slot;
DELETE FROM Salle_has_Slot;
DELETE FROM Tarif;
DELETE FROM Session;
DELETE FROM Film;
DELETE FROM Category;
DELETE FROM Category_has_Film;
DELETE FROM Age_Limit;
DELETE FROM Person;
DELETE FROM Realisator;
DELETE FROM Actor;
DELETE FROM Film_has_Actor;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Salle`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Salle` (`id`, `name`, `capacity`) VALUES (1, '01', 250);
INSERT INTO `Cinema`.`Salle` (`id`, `name`, `capacity`) VALUES (2, '02', 150);
INSERT INTO `Cinema`.`Salle` (`id`, `name`, `capacity`) VALUES (3, '03', 100);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Tarif`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Tarif` (`id`, `name`, `price`) VALUES (1, 'Tariif Plein', 980);
INSERT INTO `Cinema`.`Tarif` (`id`, `name`, `price`) VALUES (2, 'Tariff étudiant', 500);
INSERT INTO `Cinema`.`Tarif` (`id`, `name`, `price`) VALUES (3, 'Tarif Demandeur d’emploi', 500);
INSERT INTO `Cinema`.`Tarif` (`id`, `name`, `price`) VALUES (4, 'Tarif -14 ans', 380);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Version`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Version` (`id`, `name`) VALUES (1, 'VF');
INSERT INTO `Cinema`.`Version` (`id`, `name`) VALUES (2, 'VOSTFR');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Age_Limit`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Age_Limit` (`id`, `name`, `age`) VALUES (1, 'Tout public', 99);
INSERT INTO `Cinema`.`Age_Limit` (`id`, `name`, `age`) VALUES (2, 'Interdit aux moins de douze ans', 12);
INSERT INTO `Cinema`.`Age_Limit` (`id`, `name`, `age`) VALUES (3, 'Interdit aux moins de seize ans', 16);
INSERT INTO `Cinema`.`Age_Limit` (`id`, `name`, `age`) VALUES (4, 'Interdit aux mineurs', 18);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Person`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (1, 'Viggo Mortensen');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (2, 'Elijah Wood');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (3, 'Ian McKellen');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (4, 'Peter Jackson');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (5, 'Pascal Plante');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (6, 'Justine Triet');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (7, 'Juliette Gariépy');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (8, 'Maxwell McCabe-Lokos');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (9, 'Milo Machado Graner');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES (10, 'Swann Arlaud');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Realisator`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Realisator` (`Person_id`) VALUES (4);
INSERT INTO `Cinema`.`Realisator` (`Person_id`) VALUES (5);
INSERT INTO `Cinema`.`Realisator` (`Person_id`) VALUES (6);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Film`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Film` (`id`, `duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (1, 178, 'Le Seigneur des Anneaux : La communauté de l\'anneau', 'Dans ce chapitre de la trilogie, le jeune et timide Hobbit, Frodon Sacquet, hérite d\'un anneau. Bien loin d\'être une simple babiole, il s\'agit de l\'Anneau Unique, un instrument de pouvoir absolu qui permettrait à  Sauron, le Seigneur des ténèbres, de régner sur la Terre du Milieu et de réduire en esclavage ses peuples. Etc', '2001-12-19', '2024-10-03', '2025-10-03', NULL, 1, 4);
INSERT INTO `Cinema`.`Film` (`id`, `duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (2, 179, 'Le Seigneur des Anneaux : Les deux tours', 'Après la mort de Boromir et la disparition de Gandalf,  la Communauté s\'est scindée en trois. Perdus dans les collines d\'Emyn Muil  Frodon et Sam découvrent qu\'ils sont suivis par Gollum  une créature versatile corrompue par l\'Anneau etc.', '2002-12-18', '2024-10-03', '2025-10-03', NULL, 1, 4);
INSERT INTO `Cinema`.`Film` (`id`, `duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (3, 201, 'Le Seigneur des Anneaux : Le retour du roi', 'Les armées de Sauron ont attaqué Minas Tirith  la capitale de Gondor. Jamais ce royaume autrefois puissant n\'a eu autant besoin de son roi. Mais Aragorn trouvera-t-il en lui la volonté d\'accomplir sa destinée ?', '2003-12-17', '2024-10-03', NULL, NULL, 1, 4);
INSERT INTO `Cinema`.`Film` (`id`, `duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (4, 150, 'Anatomie d\'une chute', 'Sandra, Samuel et leur fils malvoyant de 11 ans, Daniel, vivent depuis un an loin de tout, à la montagne. Un jour, Samuel est etc.', '2023-08-23', '2024-10-03', NULL, 'Coup de coeur', 1, 6);
INSERT INTO `Cinema`.`Film` (`id`, `duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (5, 118, 'Les chambres rouges', 'Deux jeunes femmes se réveillent chaque matin aux portes du palais de justice pour pouvoir assister au procès hypermédiatisé', '0224-01-17', '2024-10-03', NULL, NULL, 2, 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Slot`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Slot` (`id`, `name`, `hour`) VALUES (1, 'Matin', '10:00');
INSERT INTO `Cinema`.`Slot` (`id`, `name`, `hour`) VALUES (2, 'Après-midi 1', '14:00');
INSERT INTO `Cinema`.`Slot` (`id`, `name`, `hour`) VALUES (3, 'Après-midi 2', '18:15');
INSERT INTO `Cinema`.`Slot` (`id`, `name`, `hour`) VALUES (4, 'Soirée', '20:00');
INSERT INTO `Cinema`.`Slot` (`id`, `name`, `hour`) VALUES (5, 'Nuit', '22:00');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Salle_has_Slot`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (1, 1, 1, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (2, 2, 1, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (3, 3, 1, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (4, 1, 2, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (5, 2, 2, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (6, 3, 3, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (7, 1, 4, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (8, 2, 4, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (9, 3, 5, 1);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (10, 1, 1, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (11, 2, 1, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (12, 1, 2, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (13, 2, 2, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (14, 3, 3, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (15, 1, 4, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (16, 2, 4, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (17, 3, 4, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (18, 1, 5, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (19, 2, 5, 0);
INSERT INTO `Cinema`.`Salle_has_Slot` (`id`, `Salle_id`, `Slot_id`, `isWeek`) VALUES (20, 3, 5, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Actor`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (1);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (2);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (3);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (7);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (8);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (9);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (10);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Film_has_Actor`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (1, 1);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (1, 2);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (1, 3);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (2, 1);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (2, 2);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (2, 3);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (3, 1);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (3, 2);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (3, 3);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (4, 9);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (4, 10);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (5, 7);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (5, 8);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Category`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (1, 'Drame');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (2, 'Comédie');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (3, 'Aventure');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (4, 'Animation');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (5, 'Famille');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (6, 'Romance');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (7, 'Fantastique');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (8, 'Science-fiction');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (9, 'Comédie dramatique');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (10, 'Western');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (11, 'Action');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (12, 'Documentaire');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (13, 'Policier');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (14, 'Fantasy');
INSERT INTO `Cinema`.`Category` (`id`, `name`) VALUES (15, 'Thriller');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Cinema`.`Category_has_Film`
-- -----------------------------------------------------
START TRANSACTION;
USE `Cinema`;
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (14, 1);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (5, 1);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (14, 2);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (5, 2);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (14, 3);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (5, 3);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (15, 4);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (13, 4);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (1, 4);
INSERT INTO `Cinema`.`Category_has_Film` (`Category_id`, `Film_id`) VALUES (15, 5);

COMMIT;