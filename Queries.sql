-- Partie 2 - Requêtes
use cinema;

-- Question 1 --

CREATE VIEW sessions_plan_week as
SELECT (select name from slot as s where id = slot_id) as week_slot ,
GROUP_CONCAT("Salle", name ORDER BY name) as salles from salle_has_slot ss
INNER JOIN salle ON salle.id = ss.salle_id
WHERE isWeek = 1
GROUP BY week_slot ORDER BY min(ss.slot_id);

Select * from sessions_plan_week;

CREATE VIEW sessions_plan_weekend as
SELECT (select name from slot as s where id = slot_id) as week_slot ,
GROUP_CONCAT("Salle", name ORDER BY name) as salles from salle_has_slot ss
INNER JOIN salle ON salle.id = ss.salle_id
WHERE isWeek = 0
GROUP BY week_slot ORDER BY min(ss.slot_id);

Select * from sessions_plan_weekend;

-- Qeustion 2 --
-- Ces requetes ont déjà eté effectué au niveau de l'insertion de Data.sql --

INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Viggo Mortensen');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Elijah Wood');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Ian McKellen');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Peter Jackson');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Pascal Plante');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Justine Triet');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Juliette Gariépy');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Maxwell McCabe-Lokos');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Milo Machado Graner');
INSERT INTO `Cinema`.`Person` (`id`, `name`) VALUES ('Swann Arlaud');

INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (1);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (2);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (3);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (7);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (8);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (9);
INSERT INTO `Cinema`.`Actor` (`Person_id`) VALUES (10);

INSERT INTO `Cinema`.`Realisator` (`Person_id`) VALUES (4);
INSERT INTO `Cinema`.`Realisator` (`Person_id`) VALUES (5);
INSERT INTO `Cinema`.`Realisator` (`Person_id`) VALUES (6);

-- Question 3 --
-- Ces requetes ont déjà eté effectué au niveau de l'insertion de Data.sql --

INSERT INTO `Cinema`.`Film` (`duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (178, "Le Seigneur des Anneaux : La communauté de l\'anneau', 'Dans ce chapitre de la trilogie, le jeune et timide Hobbit, Frodon Sacquet, hérite d\'un anneau. Bien loin d\'être une simple babiole, il s\'agit de l\'Anneau Unique, un instrument de pouvoir absolu qui permettrait à  Sauron, le Seigneur des ténèbres, de régner sur la Terre du Milieu et de réduire en esclavage ses peuples. Etc", '2001-12-19', '2024-10-03', '2025-10-03', NULL, 1, 4);
INSERT INTO `Cinema`.`Film` (`duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (179, "Le Seigneur des Anneaux : Les deux tours', 'Après la mort de Boromir et la disparition de Gandalf,  la Communauté s\'est scindée en trois. Perdus dans les collines d\'Emyn Muil  Frodon et Sam découvrent qu\'ils sont suivis par Gollum  une créature versatile corrompue par l\'Anneau etc.", '2002-12-18', '2024-10-03', '2025-10-03', NULL, 1, 4);
INSERT INTO `Cinema`.`Film` (`duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (201, "Le Seigneur des Anneaux : Le retour du roi', 'Les armées de Sauron ont attaqué Minas Tirith  la capitale de Gondor. Jamais ce royaume autrefois puissant n\'a eu autant besoin de son roi. Mais Aragorn trouvera-t-il en lui la volonté d\'accomplir sa destinée ?", '2003-12-17', '2024-10-03', NULL, NULL, 1, 4);
INSERT INTO `Cinema`.`Film` (`duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (150, "Anatomie d\'une chute', 'Sandra, Samuel et leur fils malvoyant de 11 ans, Daniel, vivent depuis un an loin de tout, à la montagne. Un jour, Samuel est etc.", '2023-08-23', '2024-10-03', NULL, 'Coup de coeur', 1, 6);
INSERT INTO `Cinema`.`Film` (`duration`, `title`, `synopsis`, `release_date`, `start_exploitation`, `end_exploitation`, `commentaire`, `Age_Limit_id`, `Realisator_Person_id`) VALUES (118, 'Les chambres rouges', 'Deux jeunes femmes se réveillent chaque matin aux portes du palais de justice pour pouvoir assister au procès hypermédiatisé', '0224-01-17', '2024-10-03', NULL, NULL, 2, 5);

INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (1, 1);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (1, 2);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (1, 3);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (2, 1);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (2, 2);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (2, 3);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (3, 1);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (3, 2);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (3, 3);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (4, 9);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (4, 10);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (5, 7);
INSERT INTO `Cinema`.`Film_has_Actor` (`Film_id`, `Actor_Person_id`) VALUES (5, 8);

-- Question 4 --
SELECT f.title as  'Films réalisés par Peter Jackson' from film as f
JOIN realisator as r ON f.Realisator_Person_id = r.Person_id
JOIN person as p ON r.Person_id = p.id
WHERE p.name = "Peter Jackson"
ORDER BY f.releaseDate ASC;

-- Question 5 --
SELECT f.title as "Films dans lesquels a joué Viggo Mortensen" from film as f
JOIN film_has_actor AS fa ON f.id = fa.Film_id
JOIN actor AS a ON fa.Actor_Person_id = a.Person_id
JOIN person AS p ON a.Person_id = p.id
WHERE p.name = 'Viggo Mortensen';

-- Question 6 --
SELECT f.title as "Films dans lesquels a joué Viggo Mortensen et Ian McKellen" from film as f
JOIN film_has_actor AS fa ON f.id = fa.Film_id
JOIN actor AS a ON fa.Actor_Person_id = a.Person_id
JOIN person AS p ON a.Person_id = p.id
WHERE p.name IN ('Viggo Mortensen', 'Ian McKellen')
GROUP BY f.title
HAVING COUNT(DISTINCT p.name) = 2;

-- Question 7 --
DELIMITER $$
CREATE FUNCTION format_movie_duration(duration INT)
RETURNS CHAR(5)
DETERMINISTIC
BEGIN
    DECLARE TimeFormat CHAR(5);
    SET TimeFormat = CONCAT(FLOOR(duration / 60), 'h', LPAD(duration % 60, 2, '0'));
    RETURN TimeFormat;
END;$$
DELIMITER ;

SELECT format_movie_duration(133);

-- Question 8 --
SELECT
	f.title AS Titre,
	format_movie_duration(f.duration) AS "Durée",
	f.synopsis AS Synopsis,
	p.name AS "Réalisateur",
	GROUP_CONCAT(DISTINCT pa.name ORDER BY pa.id) AS "Acteurs",
	GROUP_CONCAT(DISTINCT c.name) AS "Genres",
	f.commentaire AS Commentaire
FROM film f
INNER JOIN realisator AS r ON f.Realisator_Person_id = r.Person_id
INNER JOIN person AS p ON r.Person_id = p.id
INNER JOIN category_has_film AS cf on f.id = cf.Film_id
INNER JOIN category AS c on cf.Category_id = c.id
INNER JOIN film_has_actor AS fa ON f.id = fa.Film_id
INNER JOIN actor AS a ON fa.Actor_Person_id = a.Person_id
INNER JOIN person AS pa ON a.Person_id = pa.id
where title = "Anatomie d'une chute"
GROUP BY title, format_movie_duration(duration), synopsis, p.name, commentaire;

CREATE VIEW movie_summary AS
SELECT 
    f.title AS Titre,
    format_movie_duration(f.duration) AS "Durée",
    f.synopsis AS Synopsis,
    p.name AS "Réalisateur",
    GROUP_CONCAT(DISTINCT pa.name ORDER BY pa.id) AS "Acteurs",
    GROUP_CONCAT(DISTINCT c.name) AS "Genres",
    f.commentaire AS Commentaire
FROM film f
INNER JOIN realisator r ON f.Realisator_Person_id = r.Person_id
INNER JOIN person p ON r.Person_id = p.id
INNER JOIN category_has_film cf ON f.id = cf.Film_id
INNER JOIN category c ON cf.Category_id = c.id
INNER JOIN film_has_actor fa ON f.id = fa.Film_id
INNER JOIN actor a ON fa.Actor_Person_id = a.Person_id
INNER JOIN person pa ON a.Person_id = pa.id
GROUP BY f.title, f.duration, f.synopsis, p.name, f.commentaire;


-- Question 9 --
DELIMITER $$
CREATE PROCEDURE print_movie_summary(IN movie_title VARCHAR(100))
begin
SELECT
	f.title AS Titre,
	format_movie_duration(f.duration) AS "Durée",
	f.synopsis AS Synopsis,
	p.name AS "Réalisateur",
	GROUP_CONCAT(DISTINCT pa.name ORDER BY pa.id) AS "Acteurs",
	GROUP_CONCAT(DISTINCT c.name) AS "Genres",
	f.commentaire AS Commentaire
FROM film f 
INNER JOIN realisator AS r ON f.Realisator_Person_id = r.Person_id
INNER JOIN person AS p ON r.Person_id = p.id
INNER JOIN category_has_film AS cf on f.id = cf.Film_id
INNER JOIN category AS c on cf.Category_id = c.id
INNER JOIN film_has_actor AS fa ON f.id = fa.Film_id
INNER JOIN actor AS a ON fa.Actor_Person_id = a.Person_id
INNER JOIN person AS pa ON a.Person_id = pa.id
where title = movie_title
GROUP BY title, format_movie_duration(duration), synopsis, p.name, commentaire;
end$$
DELIMITER ;

CALL print_movie_summary("Anatomie d'une chute");
    
SELECT * FROM movie_summary;

-- Question 10 --
INSERT INTO session (`Film_id`, `Salle_has_Slot_id`, `Version_id`, `preview`, `nb_watchers`) VALUES ('1','1','1',0,0);
INSERT INTO session (`Film_id`, `Salle_has_Slot_id`, `Version_id`, `preview`, `nb_watchers`) VALUES ('1','10','1',0,0);
INSERT INTO session (`Film_id`, `Salle_has_Slot_id`, `Version_id`, `preview`, `nb_watchers`) VALUES ('4','6','1',0,0);
INSERT INTO session (`Film_id`, `Salle_has_Slot_id`, `Version_id`, `preview`, `nb_watchers`) VALUES ('4','17','1',0,0);

SELECT * from session;

-- Question 11 --
SELECT 
	f.title,
	v.name,
	CASE ss.isWeek
        WHEN 0 THEN 'weekend'
        WHEN 1 THEN 'week'
    END as Day,
    so.hour
FROM session s
INNER JOIN version v ON s.Version_id = v.id
INNER JOIN film f ON s.Film_id = f.id
INNER JOIN salle_has_slot ss ON s.Salle_has_Slot_id = ss.id
INNER JOIN salle sa ON ss.Salle_id = sa.id
INNER JOIN slot so ON ss.Slot_id = so.id
GROUP BY f.title,v.name, ss.isWeek, so.hour;

-- Question 12 --

UPDATE session
SET nb_watchers = nb_watchers + 2
WHERE id = 1;

SELECT * from session where id=1;

-- Question 13 --

SELECT sa.capacity - nb_watchers
FROM session s
INNER JOIN salle_has_slot ss ON s.Salle_has_Slot_id = ss.id
INNER JOIN salle sa ON ss.Salle_id = sa.id
where s.id =1
GROUP BY sa.capacity, nb_watchers;




