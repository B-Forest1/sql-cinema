# Table des matières

1. [Question 14](#question)
2. [Script SQL](#script)
3. [Dictionnaire](#dictionnaire)
4. [MCD](#mcd)
5. [Remarques](#remarque)

## Question

Réponse a la question 14:

Ajouter une colonne a la table salle de cinéma "isDiSpo" en boolean si il est a true on peu associer un film a cette salle. si l'heure de début de la séance + la durée du film est > a l'heure de la prochaine séance alors ca set le boolean a false et on peut pas y associer un film

## Script
Pour le premier script on va devoir créer la base de données et pour cela on va faire ma commande suivante :

```mysql -u root -p < Schema.sql``` 

en étant dans le bon dossier dans le terminal de commande.

Ensuite pour insérer les données dans la nouvelle base on va effectué la commande :

```mysql -u root -p < Data.sql```

Toujours en étant dans le bon dossier

Après pour les différentes requêtes elles sont dans le fichiers Queries.sql et au dessus de chaque requette il y a le numéro de la question qui lui est attribué.


## Dictionnaire
| Libellé                          | Code       | Type       | Obligatoire | Règle de calcul/Contrainte/Commentaire                 |
|----------------------------------|------------|------------|-------------|-------------------------------------------------------|
| Nom de la salle                  | name       | CHAR       | OUI         |                                                       |
| Capacité de la salle             | capacity   | INT        | OUI         |                                                       |
| Nom du créneau                  | name       | VARCHAR(12)| OUI         |                                                       |
| Heure du créneau                 | hour       | TIME       | OUI         | Format HH:MM                                          |
| Nom de la catégorie              | name       | VARCHAR(30)| OUI         |                                                       |
| Avant-première                   | isPreview  | BOOLEAN    | OUI         |                                                       |
| Nombre de spectateurs            | nb_watchers| INT        | OUI         | DEFAULT = 0                                           |
| Nom de la limite d'âge           | name       | VARCHAR(35)| OUI         |                                                       |
| Âge limite                       | age        | INT        | OUI         |                                                       |
| Nom d'une personne               | name       | VARCHAR(50)| OUI         |                                                       |
| Durée d'un film                  | duration   | INT        | OUI         |                                                       |
| Titre d'un film                  | title      | VARCHAR(100)| OUI        |                                                       |
| Synopsis d'un film               | synopsis   | LONGTEXT   | OUI         |                                                       |
| Date de sortie                   | release_date| DATE      | OUI         | Format YYYY-MM-DD                                     |
| Date début d'exploitation        | start_exploitation| DATE | OUI         | Format YYYY-MM-DD. Si nulle, c'est égal à Date début d'exploitation + 8 semaines |
| Date fin d'exploitation          | end_exploitation | DATE | NON         | Format YYYY-MM-DD                                     |
| Commentaire sur un film          | commentaire| VARCHAR(55)| NON         |                                                       |
| Nom du tarif                     | name       | VARCHAR(25)| OUI         |                                                       |
| Prix du tarif                    | price      | INT        | OUI         |                                                       |


## MCD
Et voici le MCD que j'ai fait sur MYSQL Workbench

![MCD](MCD.png)

## Remarque

J'ai eu une difficulté sur la gestion des tarifs et des billets.

Car j'ai gérer le nombre de spectateurs juste avec une variable dans la table Session.

Et je savais pas trop avec quelle table liée avec tarifs si c'était le film qui avait certains tarifs comme il y a des limite d'age 

Mais du coup il aurait fallu surement créer une table billet qui sera lié au tarif et lié a une session

qui permettra aussi de compté de facon plus claire le nombre de spectateur pour la question 12 et 13


