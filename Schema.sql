-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Cinema
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Cinema` ;

-- -----------------------------------------------------
-- Schema Cinema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Cinema` DEFAULT CHARACTER SET utf8 ;
USE `Cinema` ;

-- -----------------------------------------------------
-- Table `Cinema`.`Salle`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Salle` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Salle` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` CHAR(2) NOT NULL,
  `capacity` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Salle` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Tarif`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Tarif` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Tarif` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `price` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Tarif` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Version`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Version` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Version` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Version` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Age_Limit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Age_Limit` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Age_Limit` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `age` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Age_Limit` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Person` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Person` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Person` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Realisator`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Realisator` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Realisator` (
  `Person_id` BIGINT NOT NULL,
  PRIMARY KEY (`Person_id`),
  CONSTRAINT `fk_realisator_Person1`
    FOREIGN KEY (`Person_id`)
    REFERENCES `Cinema`.`Person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_realisator_Person1_idx` ON `Cinema`.`Realisator` (`Person_id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Film`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Film` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Film` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `duration` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `synopsis` LONGTEXT NOT NULL,
  `release_date` DATE NOT NULL,
  `start_exploitation` DATE NOT NULL,
  `end_exploitation` DATE NULL,
  `commentaire` VARCHAR(55) NULL,
  `Age_Limit_id` BIGINT NOT NULL,
  `Realisator_Person_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Film_Age_Limit1`
    FOREIGN KEY (`Age_Limit_id`)
    REFERENCES `Cinema`.`Age_Limit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Film_Realisator1`
    FOREIGN KEY (`Realisator_Person_id`)
    REFERENCES `Cinema`.`Realisator` (`Person_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Film` (`id` ASC);

CREATE INDEX `fk_Film_Age_Limit1_idx` ON `Cinema`.`Film` (`Age_Limit_id` ASC);

CREATE INDEX `fk_Film_Realisator1_idx` ON `Cinema`.`Film` (`Realisator_Person_id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Slot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Slot` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Slot` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(15) NOT NULL,
  `hour` TIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Slot` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Salle_has_Slot`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Salle_has_Slot` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Salle_has_Slot` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `Salle_id` BIGINT NOT NULL,
  `Slot_id` BIGINT NOT NULL,
  `isWeek` TINYINT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Salle_has_Slot_Salle1`
    FOREIGN KEY (`Salle_id`)
    REFERENCES `Cinema`.`Salle` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Salle_has_Slot_Slot1`
    FOREIGN KEY (`Slot_id`)
    REFERENCES `Cinema`.`Slot` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Salle_has_Slot_Slot1_idx` ON `Cinema`.`Salle_has_Slot` (`Slot_id` ASC);

CREATE INDEX `fk_Salle_has_Slot_Salle1_idx` ON `Cinema`.`Salle_has_Slot` (`Salle_id` ASC);

CREATE UNIQUE INDEX `Salle_has_Slotcol_UNIQUE` ON `Cinema`.`Salle_has_Slot` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Session`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Session` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Session` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `Film_id` BIGINT NOT NULL,
  `Salle_has_Slot_id` BIGINT NOT NULL,
  `Version_id` BIGINT NOT NULL,
  `preview` TINYINT NOT NULL,
  `nb_watchers` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Salle_has_Film_Film1`
    FOREIGN KEY (`Film_id`)
    REFERENCES `Cinema`.`Film` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Salle_has_Film_Version1`
    FOREIGN KEY (`Version_id`)
    REFERENCES `Cinema`.`Version` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Session_Salle_has_Slot1`
    FOREIGN KEY (`Salle_has_Slot_id`)
    REFERENCES `Cinema`.`Salle_has_Slot` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Salle_has_Film_Film1_idx` ON `Cinema`.`Session` (`Film_id` ASC);

CREATE INDEX `fk_Salle_has_Film_Version1_idx` ON `Cinema`.`Session` (`Version_id` ASC);

CREATE INDEX `fk_Session_Salle_has_Slot1_idx` ON `Cinema`.`Session` (`Salle_has_Slot_id` ASC);

CREATE UNIQUE INDEX `id_UNIQUE` ON `Cinema`.`Session` (`id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Actor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Actor` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Actor` (
  `Person_id` BIGINT NOT NULL,
  PRIMARY KEY (`Person_id`),
  CONSTRAINT `fk_actor_Person1`
    FOREIGN KEY (`Person_id`)
    REFERENCES `Cinema`.`Person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_actor_Person1_idx` ON `Cinema`.`Actor` (`Person_id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Film_has_Actor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Film_has_Actor` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Film_has_Actor` (
  `Film_id` BIGINT NOT NULL,
  `Actor_Person_id` BIGINT NOT NULL,
  PRIMARY KEY (`Film_id`, `Actor_Person_id`),
  CONSTRAINT `fk_Film_has_Actor_Film1`
    FOREIGN KEY (`Film_id`)
    REFERENCES `Cinema`.`Film` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Film_has_Actor_Actor1`
    FOREIGN KEY (`Actor_Person_id`)
    REFERENCES `Cinema`.`Actor` (`Person_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Film_has_Actor_Actor1_idx` ON `Cinema`.`Film_has_Actor` (`Actor_Person_id` ASC);

CREATE INDEX `fk_Film_has_Actor_Film1_idx` ON `Cinema`.`Film_has_Actor` (`Film_id` ASC);


-- -----------------------------------------------------
-- Table `Cinema`.`Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Category` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Category` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Cinema`.`Category_has_Film`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Cinema`.`Category_has_Film` ;

CREATE TABLE IF NOT EXISTS `Cinema`.`Category_has_Film` (
  `Category_id` BIGINT NOT NULL,
  `Film_id` BIGINT NOT NULL,
  PRIMARY KEY (`Category_id`, `Film_id`),
  CONSTRAINT `fk_Category_has_Film_Category1`
    FOREIGN KEY (`Category_id`)
    REFERENCES `Cinema`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Category_has_Film_Film1`
    FOREIGN KEY (`Film_id`)
    REFERENCES `Cinema`.`Film` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Category_has_Film_Film1_idx` ON `Cinema`.`Category_has_Film` (`Film_id` ASC);

CREATE INDEX `fk_Category_has_Film_Category1_idx` ON `Cinema`.`Category_has_Film` (`Category_id` ASC);

USE `Cinema`;

DELIMITER $$

USE `Cinema`$$
DROP TRIGGER IF EXISTS `Cinema`.`Film_BEFORE_INSERT` $$
USE `Cinema`$$
CREATE DEFINER = CURRENT_USER TRIGGER `Cinema`.`Film_BEFORE_INSERT` BEFORE INSERT ON `Film` FOR EACH ROW
BEGIN
IF NEW.end_exploitation IS NULL THEN
        SET NEW.end_exploitation = DATE_ADD(NEW.start_exploitation, INTERVAL 8 WEEK);
    END IF;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
